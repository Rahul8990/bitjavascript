const delayms = 1;

function getCurrentCity(callback) {
  setTimeout(function () {

    const city = "New York, NY";
    callback(null, city);

  }, delayms)
}

function getWeather(city, callback) {
  setTimeout(function () {

    if (!city) {
      callback(new Error("City required to get weather"));
      return;
    }

    const weather = {
      temp: 50
    };

    callback(null, weather)

  }, delayms)
}

function getForecast(city, callback) {
  setTimeout(function () {

    if (!city) {
      callback(new Error("City required to get forecast"));
      return;
    }

    const fiveDay = {
      fiveDay: [60, 70, 80, 45, 50]
    };

    callback(null, fiveDay)

  }, delayms)
}
suite.only("operations");
function Operation() 
{
  const operation = {
    successReaction: [],
    failureReaction: []
  };
  operation.fail = function fail(error) {
    operation.state ="failed";
    operation.error = error;
    operation.failureReaction.forEach(r => r(error))
  }
  operation.succeed = function succeed(result)
  {
    operation.state ="Succeeded";
    operation.result = result;
    operation.successReaction.forEach(r=> r(result));
  }
  operation.onCompletion = function onCompletion(onSuccess, onError){
    const noop = function() {}
    const completionOpp = new Operation();
    function successHandler(){
      if(onSuccess) {
        const callbackResult = onSuccess(operation.result);
        if(callbackResult && callbackResult.onCompletion){
          callbackResult.forwardCompletions(completionOpp)
        }
      }
    }
    if(operation.state == 'Succeeded'){
     successHandler();
    }
    else if(operation.state == 'failed'){
      onError(operation.error);
    }
    else{
      operation.successReaction.push(successHandler || noop);
      operation.failureReaction.push(onError || noop);
    }
    return completionOpp;
  }
  operation.then = operation.onCompletion;
  operation.onFailure = function onFailure(onError){
   return  operation.onCompletion(null, onError);
  }
  operation.nodeCallback = function(error, result) {
    //onError('regardless')
    if(error){
    operation.fail(error)
      return;
    }
   operation.succeed(result);
  };
  operation.forwardCompletions = function(op){
    operation.onCompletion(op.succeed, op.fail)
  };
  return operation;
}


function fetchtForcast(city)
{
  const operation = new Operation()
  getForecast(city, operation.nodeCallback);
  return operation;
}
function fetchCurrentCity(){
  const operation = new Operation();
  getCurrentCity(operation.nodeCallback);
  return operation;
}

function fetchWeather(city){
  const operation = new Operation();
  getWeather(city,operation.nodeCallback);
 
  return operation;

}
function doLater(func)
{
  setTimeout(func, 1);
}
test(" register error callback async", function(done){
  var operationThatHaveError = fetchWeather();
  doLater(function() {
    operationThatHaveError.onFailure(() => done())
  });

})
test(" register success callback async", function(done){
  var currentCity = fetchCurrentCity();
  doLater(function() {
    currentCity.onCompletion(() => done())
  });

})
test('life is full of async, nesting is inevtiable  let"s do something about it', (done) => {
  fetchCurrentCity()
      .then(city => fetchWeather(city))
      .then(weather => done());
  })
  
test('laxical parallelist', (done) => {
  const city = 'NYC';
  const weatherOpp = fetchWeather(city);
  const forcastOpp = fetchtForcast(city);
  weatherOpp.onCompletion(function(weather){
    forcastOpp.onCompletion(function(forecset){
        console.log(`it is currents  ${weather.temp} with a five day forecaset`);
        done();
    });
  });
})

test("summary", function(){
  fetchCurrentCity().onCompletion(function(city){
    console.log(city);
    fetchWeather(city).onCompletion(function(weather){
    console.log(weather);
    });
    const forecast = fetchtForcast(city);
    forecast.onCompletion(function(forecast){
      console.log(forecast)
    });
    forecast.onCompletion(function(forecast){
      console.log(forecast);
    });
  });
});
test('register only error handler, ignore success handler', function(done){
  const operation = fetchCurrentCity();
  operation.onFailure( result => done(error));
  operation.onCompletion(result => done());
  return operation;

})
test('register only error handler, ignore errir handler', function(done){
  const operation = fetchWeather();
  operation.onCompletion(result => done(new Error('should succeed')));
  operation.onFailure( result => done());

})
test('pass multiple call back --all of them are called', function(done){
  const operation = fetchCurrentCity();
  multiDone = callDone(done).afterTwoCalls();
  operation.onCompletion(result => multiDone());
  operation.onCompletion(result => multiDone());
})
